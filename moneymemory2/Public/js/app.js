function showRequirements() {
    document.getElementById("passwordRequirements").style.display = "block";
}

function hideRequirements() {
    document.getElementById("passwordRequirements").style.display = "none";
}

function validatePassword() {
    var format = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{4,64})/g;
    if (!format.test(document.getElementById("psw").value)) {
        alert("Hasło nie spełnia wymagań!");
        return false;
    }
    return true;
}

function passwordVisibility() {
    document.getElementById("psw").type = "text";
}

function getExpenses() {
    const apiUrl = "http://localhost:8000";
    const $list = $('.expenses-list');

    $.ajax({
        url: apiUrl + '/?page=getExpenses',
        dataType: 'json'
    })
        .done((res) => {
            $list.empty();

            res.forEach(el => {
                $list.append(`<tr>
                            <td>${el.name}</td>
                            <td>${el.date}</td>
                            <td>${el.amount}</td>
                            <td>${el.currency}</td>
                            </tr>`);
            });
        });
}

function getUsers() {
    const apiUrl = "http://localhost:8000";
    const $list = $('.users-list');

    $.ajax({
        url: apiUrl + '/?page=getUsers',
        dataType: 'json'
    })
        .done((res) => {
            $list.empty();

            res.forEach(el => {
                $list.append(`<tr>
                            <td>${el.id}</td>
                            <td>${el.email}</td>
                            <td>${el.name}</td>
                            <td>${el.surname}</td>
                            <td>${el.role}</td>
                            <td>
                                <form action="?page=changeRole" method="post">
                                    <button class="changeRole-btn" name="email" value="${el.email}">Zmień uprawnienia</button>
                                </form>
                                <form action="?page=deleteUser" method="post" class="formDel">
                                    <button class="deleteUser-btn" name="email" value="${el.email}">Usuń</button>
                                </form>
                            </td>
                            </tr>`);
            });
        });
}
