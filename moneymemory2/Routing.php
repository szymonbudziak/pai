<?php

require_once 'Controllers//SecurityController.php';
require_once 'Controllers//ExpenseController.php';
require_once 'Controllers//AdminController.php';

class Routing
{
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'register' => [
                'controller' => 'SecurityController',
                'action' => 'register'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'newExpense' => [
                'controller' => 'ExpenseController',
                'action' => 'postExpense'
            ],
            'expenses' => [
                'controller' => 'ExpenseController',
                'action' => 'expenses'
            ],
            'getExpenses' => [
                'controller' => 'ExpenseController',
                'action' => 'getExpenses'
            ],
            'admin' => [
                'controller' => 'AdminController',
                'action' => 'index'
            ],
            'getUsers' => [
                'controller' => 'AdminController',
                'action' => 'getUsers'
            ],
            'changeRole' => [
                'controller' => 'AdminController',
                'action' => 'changeRole'
            ],
            'deleteUser' => [
                'controller' => 'AdminController',
                'action' => 'deleteUser'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'login';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}