<?php

require_once "Repository.php";
require_once __DIR__ . '//..//Models//Expense.php';

class ExpenseRepository extends Repository
{

    public function postExpense(string $userId, string $name, string $date, int $amount, string $currency, string $description)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO expenses (id_user, name, date, amount, currency, description)
            VALUES (:userId, :name, :date, :amount, :currency, :description)
        ');

        $stmt->bindParam(':userId', $userId, PDO::PARAM_STR);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':date', $date, PDO::PARAM_STR);
        $stmt->bindParam(':amount', $amount, PDO::PARAM_STR);
        $stmt->bindParam(':currency', $currency, PDO::PARAM_STR);
        $stmt->bindParam(':description', $description, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function getExpenses(): array
    {
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM expenses WHERE id_user like :userId;
        ');

        $stmt->bindParam(':userId', $_SESSION['id'], PDO::PARAM_STR);
        $stmt->execute();
        $expenses = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $expenses;
    }

    public function deleteExpenses(string $email)
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM expenses WHERE id_user like :email;
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
    }

}