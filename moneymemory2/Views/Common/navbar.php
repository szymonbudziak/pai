<nav class="navbar">
    <a class="navbar-brand" href="#">MoneyMemory</a>
    <ul class="navbar-ul">
        <?php if ($_SESSION['role'] == 'admin'): ?>
            <li class="navbar-item">
                <a class="navbar-link" href="?page=admin">ADMIN</a>
            </li>
        <?php endif ?>
        <li class="navbar-item">
            <a class="navbar-link" href="#">USTAWIENIA</a>
        </li>
        <li class="navbar-item">
            <a class="navbar-link" href="#">O APLIKACJI</a>
        </li>
        <li class="navbar-item">
            <a class="navbar-link" href="?page=logout">WYLOGUJ SIĘ</a>
        </li>
    </ul>
</nav>