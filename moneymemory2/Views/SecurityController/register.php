<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css"/>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <?php include(dirname(__DIR__) . '/Common/head.php'); ?>
    <title>MoneyMemory</title>
</head>
<body>
<nav class="navbar">
    <a class="navbar-brand" href="#">MoneyMemory</a>
    <a href="#">Rejestracja</a>
</nav>
<div class="container">
    <form action="?page=register" method="POST" onsubmit="return validatePassword()">
        <div class="messages">
            <p class="loginMess">ZAREJESTRUJ SIĘ</p>
            <?php
            if (isset($messages)) {
                foreach ($messages as $message) {
                    echo $message;
                }
            }
            ?>
        </div>
        <input name="email" type="email" required placeholder="email@email.com">
        <input name="password" type="password" required placeholder="hasło" id="psw" onfocus="showRequirements()"
               onblur="hideRequirements()">
        <ul id="passwordRequirements">
            <li style="list-style:none"></li>
            <li>Minimum 4 znaki</li>
            <li>Minimum jedna wielka litera</li>
            <li>Minimum jedna mała litera</li>
            <li>Minimum jedna cyfra</li>
            <li>Minimum jeden znak specjalny</li>
        </ul>
        <input name="name" type="text" placeholder="imię">
        <input name="surname" type="text" placeholder="nazwisko">
        <button type="submit">ZAŁÓŻ KONTO</button>
        <div class="messages">
            <p>Posiadasz konto? <a href="?page=login">Zaloguj się</a></p>
        </div>
    </form>
</div>
</body>
</html>