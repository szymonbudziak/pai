<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css"/>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <title>MoneyMemory</title>
</head>
<body>
<nav class="navbar">
    <a class="navbar-brand" href="#">MoneyMemory</a>
    <a href="#">Rejestracja</a>
</nav>
<div class="container">
    <form action="?page=login" method="POST">
        <div class="messages">
            <p class="loginMess">ZALOGUJ SIĘ</p>
            <?php
            if (isset($messages)) {
                foreach ($messages as $message) {
                    echo $message;
                }
            }
            ?>
        </div>
        <input name="email" type="text" placeholder="email@email.com">
        <input name="password" type="password" placeholder="password">
        <button type="submit">KONTYNUUJ</button>
        <div class="messages">
            <p>Nie posiadasz konta? <a href="?page=register">Zarejestruj się</a></p>
        </div>
    </form>
</div>
</body>
</html>