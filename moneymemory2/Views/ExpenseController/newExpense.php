<?php
require_once "isLogged.php";
?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css"/>
    <link rel="Stylesheet" type="text/css" href="../Public/css/newExpense.css"/>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <?php include(dirname(__DIR__) . '/Common/head.php'); ?>
    <title>MoneyMemory</title>
</head>
<body>
<?php include(dirname(__DIR__) . '/Common/navbar.php'); ?>
<div class="container">
    <div class="row row-main">
        <div class="col-sm-2 sidebar">
            <ul class="sidebar-ul">
                <li class="sidebar-li">
                    <a href="?page=newExpense">Nowy wydatek</a>
                </li>
                <li class="sidebar-li">
                    <a href="?page=expenses">Wydatki</a>
                </li>
                <li class="sidebar-li">
                    <a href="#">link3</a>
                </li>
                <li class="sidebar-li">
                    <a href="#">link4</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-10">
            <div class="container">
                <h2>Nowy wydatek</h2>
                <form action="?page=newExpense" method="POST">
                    <input name="name" type="text" placeholder="wydatek">
                    <input name="date" type="date">
                    <input name="amount" type="number" placeholder="kwota">
                    <select name="currency">
                        <option value="PLN">PLN</option>
                        <option value="EUR">EUR</option>
                        <option value="USD">USD</option>
                        <option value="GBO">GBO</option>
                    </select>
                    <input id="desc" name="description" type="textarea" placeholder="opis">
                    <button type="submit">Dodaj wydatek</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 bottom-sm">
            <a href="#"><i class="fab fa-facebook-square"></i></a>
            <a href="#"><i class="fab fa-instagram"></i></a>
            <a href="#"><i class="fab fa-twitter-square"></i></a>
        </div>
    </div>
</div>
</body>
</html>