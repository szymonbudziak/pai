<?php
require_once "isLogged.php";
?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="Stylesheet" type="text/css" href="../Public/css/style.css"/>
    <link rel="Stylesheet" type="text/css" href="../Public/css/expenses.css"/>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <?php include(dirname(__DIR__) . '/Common/head.php'); ?>
    <title>MoneyMemory</title>
</head>
<body>
<?php include(dirname(__DIR__) . '/Common/navbar.php'); ?>
<div class="container">
    <div class="row row-main">
        <div class="col-sm-2 sidebar">
            <ul class="sidebar-ul">
                <li class="sidebar-li">
                    <a href="?page=newExpense">Nowy wydatek</a>
                </li>
                <li class="sidebar-li">
                    <a href="?page=expenses">Wydatki</a>
                </li>
                <li class="sidebar-li">
                    <a href="#">link3</a>
                </li>
                <li class="sidebar-li">
                    <a href="#">link4</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-10">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Wydatek</th>
                    <th scope="col">Data</th>
                    <th scope="col">Kwota</th>
                    <th scope="col">Waluta</th>
                </tr>
                </thead>
                <tbody class="expenses-list">
                </tbody>
            </table>
            <button type="button" onclick="getExpenses()">
                Pokaż wydatki
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 bottom-sm">
            <a href="#"><i class="fab fa-facebook-square"></i></a>
            <a href="#"><i class="fab fa-instagram"></i></a>
            <a href="#"><i class="fab fa-twitter-square"></i></a>
        </div>
    </div>
</div>
</body>
</html>