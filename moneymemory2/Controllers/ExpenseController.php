<?php

require_once 'AppController.php';
require_once __DIR__ . '//..//Models//Expense.php';
require_once __DIR__ . '//..//Repository//ExpenseRepository.php';

class ExpenseController extends AppController
{

    public function postExpense()
    {
        $expenseRepository = new ExpenseRepository();

        if ($this->isPost()) {
            $name = $_POST['name'];
            $date = $_POST['date'];
            $amount = $_POST['amount'];
            $currency = $_POST['currency'];
            $description = $_POST['description'];

            $time = strtotime($date);
            $date2 = date('Y-m-d', $time);
            $userId = $_SESSION['id'];
            $expense = $expenseRepository->postExpense($userId, $name, $date2, $amount, $currency, $description);
        }
        $this->render('newExpense');
    }

    public function expenses()
    {
        $this->render('expenses');
    }

    public function getExpenses()
    {
        $expenseRepository = new ExpenseRepository();

        header('Content-type: application/json');
        http_response_code(200);

        $expenses = $expenseRepository->getExpenses();
        echo $expenses ? json_encode($expenses) : '';
    }

}