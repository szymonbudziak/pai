<?php

require_once 'AppController.php';
require_once __DIR__ . '//..//Models//User.php';
require_once __DIR__ . '//..//Repository//UserRepository.php';
require_once __DIR__ . '//..//Repository//ExpenseRepository.php';

class AdminController extends AppController
{

    public function index()
    {
        $userRepository = new UserRepository();
        $this->render('users', ['user' => $userRepository->getUser($_SESSION['id'])]);
    }

    public function getUsers()
    {
        $userRepository = new UserRepository();

        header('Content-type: application/json');
        http_response_code(200);

        $users = $userRepository->getUsers();
        echo $users ? json_encode($users) : '';
    }

    public function changeRole()
    {
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $email = $_POST['email'];

            $user = $userRepository->getUser($email);
            if ($user->getRole() == "user") {
                $userRepository->changeRole($email, "admin");
            } else {
                $userRepository->changeRole($email, "user");
            }
        }
        $this->render('users', ['user' => $userRepository->getUser($_SESSION['id'])]);
    }

    public function deleteUser()
    {
        $userRepository = new UserRepository();
        $expenseRepository = new ExpenseRepository();

        if ($this->isPost()) {
            $email = $_POST['email'];

            $userRepository->deleteUser($email);

        }
        $this->render('users', ['user' => $userRepository->getUser($_SESSION['id'])]);
    }
}