<?php

require_once 'AppController.php';
require_once __DIR__ . '//..//Models//User.php';
require_once __DIR__ . '//..//Repository//UserRepository.php';

class SecurityController extends AppController
{

    public function login()
    {
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $userRepository->getUser($email);

            if ($user->getEmail() !== $email) {
                $this->render('login', ['messages' => ['User with this email not exist!']]);
                return;
            }

            if ($user->getPassword() !== $password) {
                $this->render('login', ['messages' => ['Wrong password!']]);
                return;
            }


            $_SESSION["id"] = $user->getEmail();
            $_SESSION["role"] = $user->getRole();

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}?page=expenses");
        }

        $this->render('login');
    }

    public function register()
    {
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            $name = $_POST['name'];
            $surname = $_POST['surname'];

            $user = $userRepository->getUser($email);

            if ($user !== null) {
                $this->render('register', ['messages' => ['Podany email jest już zarejestrowany']]);
                return;
            }

            $user = $userRepository->registerUser($email, $password, $name, $surname);
            $this->render('login', ['messages' => ['Możesz się teraz zalogować']]);
        }
        $this->render('register');
    }

    public function logout()
    {
        session_unset();
        session_destroy();

        $this->render('login', ['messages' => ['Wylogowałeś się']]);
    }
}