<?php
if (!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
    die('Nie jesteś zalogowany');
}

if ($_SESSION['role'] != 'admin') {
    die('Potrzebujesz uprawnień administratora');
}
?>