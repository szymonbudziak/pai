<?php

class Expense
{
    private $name;
    private $date;
    private $amount;
    private $currency;
    private $description;

    public function __construct(string $name, string $date, int $amount, string $currency, string $description)
    {
        $this->name = $name;
        $this->date = $date;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->description = $description;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCurrency(): string
    {
        return $this->curency;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}